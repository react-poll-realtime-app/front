var socket = require('socket.io-client')('http://localhost:8080');

export default socket;

export const enterPoll = (pollId, userName) => {
  console.log("Hey sone, i want to enter the poll");
  socket.emit('enter', {
    pollId,
    userName
  })
}
export const subscribeToPoll = (pollId) => {
  socket.emit('subscribe_poll', {pollId});
}
export const addOption = (pollId, option) => {
  socket.emit('add_option', {
    pollId,
    option
  })
}
export const emitStartPoll = (pollId) => {
  socket.emit('start_poll', {
    pollId
  })
}
export const emitPollChangeStatus = (pollId, status) => {
  socket.emit('poll_status_change', {
    pollId,
    status
  })
}
export const emitChooseOption = (pollId, index, user) => {
  socket.emit('poll_option_choose', {
    pollId,
    index,
    user
  })
}
