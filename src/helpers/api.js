var server = {};
var apiUrl = 'http://localhost:8080/api/polls/';
var axios = require('axios');

var generateRequest = (method, data) => {
  return ({
    method: method,
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      "Access-Control-Allow-Origin": "*"
    },
    // mode: 'no-cors',
    body: JSON.stringify(data)
  });
};

const getPostRequest = ((data = {}) => {
  return (generateRequest('POST', data));
});

const getPutRequest = ((data = {}) => {
  return (generateRequest('PUT', data));
});

const getDeleteRequest = ((data) => {
  return (generateRequest('DELETE', data));
});

server.createPoll = function(user,description){
  var data = {user, description};
  return axios.post(apiUrl, {
    user,
    description
  });
};
server.getPoll = (id) => {
  return axios.get(apiUrl  + id);
}
server.joinPoll = (id,myName) => {
  return axios.post(apiUrl + id + "/join", {
    id,
    user: myName
  });
}

module.exports = server;
