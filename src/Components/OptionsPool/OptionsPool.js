import React from 'react';

const OptionsPool = ({options}) => (
  <div
    style={{display: 'inline-block', padding: 10,
    borderWidth: 1, borderColor: 'blue', borderStyle: 'solid'}}>
    {options.map((opt, index) => (
      <span key={index}>
        {opt}
        {index !== options.length-1 && <font>, </font>}
      </span>
    ))}
  </div>
)

export default OptionsPool;
