import React from 'react';

const ListOptions = ({options = []}) => {
  return(
    <div>
      <h4>Options:</h4>
      <ul>
        {options.map((opt, i) => (
          <li key={i}>
            {opt}
          </li>
        ))}
      </ul>
    </div>
  )
}

export default ListOptions;
