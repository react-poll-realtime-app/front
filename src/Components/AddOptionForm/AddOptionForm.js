import React from 'react';


const AddOptionForm = ({onAdd}) => {
  let userInput;

  return (
    <div>
      <form onSubmit={(e) => {
        e.preventDefault();
        onAdd(userInput.value)
        userInput.value = "";
      }}>
        <input type="text" ref={r => userInput = r} />
        <input type="submit" value="Add" />
      </form>

    </div>
  )
}

export default AddOptionForm;
