import React from 'react';

const JoinPoll = ({onJoinPress}) => {
  let idForm;
  return (
    <form onSubmit={(e) => {
      e.preventDefault();
      onJoinPress(idForm.value);
      idForm.value = "";
    }}>
      <input ref={r => idForm = r} type="text" placeholder="Example: #147" />
      <input type="submit" value="Join" />
    </form>
  )
}

export default JoinPoll;
