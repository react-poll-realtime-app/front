import React from 'react';

const ListUsers = ({users = []}) => {
  return(
    <div>
      <h2>Users in poll: </h2>
      <ul>
        {users.map((user, i) => (
          <li key={i}>
            {user}
          </li>
        ))}
      </ul>
    </div>
  )
}

export default ListUsers;
