import React from 'react';
import ListOptions from './../../Components/ListOptions';
import ListUsers from './../../Components/ListUsers';
import AddOptionForm from './../../Components/AddOptionForm';


const PollCreateMode = ({options, users, onOptionAdded, onStartPressed, isAdmin}) => {
  return (
    <div>
      <div style={{borderWidth: 1, borderColor: 'blue'}}>
        <ListOptions options={options} />
        <AddOptionForm onAdd={(newOpt) => onOptionAdded(newOpt)} />
      </div>
      <ListUsers users={users} />
      {isAdmin && <input type="button" value="Start" onClick={() => onStartPressed("IN_GAME") } />}

    </div>
  )
}

export default PollCreateMode;
