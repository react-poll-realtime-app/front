import React from 'react';

const CreatePoll = ({onPollCreate}) => {
  let iDesc;
  return (
    <form onSubmit={(e) => {
      e.preventDefault();
      var desc = iDesc.value;
      onPollCreate(desc);
      iDesc.value = "";
    }}>
      <input ref={r => iDesc = r} type="text" placeholder="poll description" />
      <input type="submit" value="Create Poll"/>
    </form>

  )
}

export default CreatePoll;
