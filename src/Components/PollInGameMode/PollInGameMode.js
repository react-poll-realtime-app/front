import React from 'react';

import OptionsPool from './../OptionsPool';

const VoteButton = ({vote, onPressButton, hasVoted}) => (
  <div style={{height: 100, width: 100, backgroundColor: hasVoted ? 'green' : 'grey', margin: 10}}>
    <div>
      <button style={{width: 100, height: 20}} onClick={() => onPressButton()} >
        {vote.name}
      </button>
      <ul>
        {vote.voters.map((user, index) => (
          <li key={index}>{user}</li>
        ))}
      </ul>
    </div>
  </div>
);

const PollinGameMode = ({votes, onChoose, currUser, options, users}) => {
  return (
    <div>
      <div>
        Users:
        {users.map((u, index) => <li key={index}>{u}</li>)}
      </div>
      <div>
        <h3>Choose an option:</h3>
        <div style={{display: 'flex', flexDirection: 'row'}}>
          <VoteButton
            hasVoted={isUserVoted(currUser, votes, 0)}
            vote={votes[0]}
            onPressButton={() => onChoose(0)} />
          <VoteButton
            hasVoted={isUserVoted(currUser, votes, 1)}
            vote={votes[1]}
            onPressButton={() => onChoose(1)} />
        </div>
      </div>
      <div>
        {options.length > 0 &&
          <div>
            <h4>Options pool:</h4>
            <OptionsPool options={options} />
          </div>
        }
      </div>

    </div>
  )
}

const isUserVoted = (user, votes, index) => (votes[index].voters.filter((v) => v == user).length > 0);

export default PollinGameMode;
