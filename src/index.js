import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './index.css';
import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom'

import HomeView from './Views/HomeView';
import PollView from './Views/PollView';

const Routes = () => (
  <Router>
    <div>
      <Route exact path="/" component={HomeView} />
      <Route path="/poll/:id" component={PollView} />
    </div>
  </Router>
);

ReactDOM.render(
  <Routes />,
  document.getElementById('root')
);
