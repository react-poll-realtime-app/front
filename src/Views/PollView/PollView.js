import React from 'react';
import socket,
{addOption as emitSocketOption,
  subscribeToPoll,
  emitStartPoll,
  emitChooseOption} from './../../helpers/socket';
import PollCreateMode from './../../Components/PollCreateMode';
import PollinGameMode from './../../Components/PollInGameMode';
// var socket = require('socket.io-client')('http://localhost:8080');
// import io from 'socket.io-client';
// let socket = io('http://localhost:8080');

var api = require('./../../helpers/api');

class PoleView extends React.Component{
  constructor(props){
    super(props);
    this.state = {
      lastLogin: '',
      poll: {},
      myName: props.myName
    }

    this.pollId = this.props.match.params.id;
  }

  componentDidMount(){
    api.getPoll(this.pollId)
    .then(poll => {
      this.setState({poll: poll.data})
    });

    subscribeToPoll(this.pollId);
    socket.on('userJoined', (uName) => this.addUserToList(uName))
    socket.on('optionAdded', (option) => this.addOptionToList(option))
    socket.on('pollChanged', (poll) => {
      console.log("POLL_HAS_CHANGED", poll);
      this.setState({poll: poll});
    })
    socket.on('pollStatusChanged', (newStatus) => {
      console.log("POLE_CHANGES");
      this.setState({
        poll: {
          ...this.state.poll,
          status: newStatus
        }
      })
    })
  }

  componentWillUnmount(){
    socket.removeListener('userJoined');
    socket.removeListener('optionAdded');
    socket.removeListener('pollChanged');
    socket.removeListener('pollStatusChanged');
  }

  addUserToList = (user) => {
    console.log("ADD_USER_LIST");
    var {poll} = this.state;
    let newUsers = poll.users.concat(user);
    this.setState({
      poll: {
        ...poll,
        users: newUsers
      }
    })
  }

  addOptionToList = (option) => {
    console.log("ADD_OPTIONS_LIST");
    const {poll} = this.state;
    this.setState({
      poll: {
        ...poll,
        options: poll.options.concat(option)
      }
    })
  }

  render(){
    var {description, users, options, _id,
        admin, status, inDuel, winner} = this.state.poll;
    var propsState = this.props.location.state;
    var myName = (propsState) ? propsState.myName : '';
    console.log("ADMIN", admin, myName);
    var isAdmin = (myName == admin);
    var adminText = (isAdmin) ? '(admin)' : '';

    return (
      <div>
        <h2>{`Hey ${myName}  ${adminText}`}</h2>
        <h1>
          {`${description} (#${_id})`}
        </h1>
        {status == "CREATED" &&
        <PollCreateMode
          {...this.state.poll}
          isAdmin={isAdmin}
          onOptionAdded={(newOpt) =>  emitSocketOption(this.pollId, newOpt)}
          onStartPressed={() => emitStartPoll(this.pollId)}
         />}
        {status == "IN_GAME" &&
          <PollinGameMode
            users={users}
            options={options}
            currUser={myName}
            votes={inDuel.options}
            onChoose={(index) => emitChooseOption(this.pollId, index, myName)} />}
        {status == "FINISHED" &&
          <div>{`Finished, the winner is ${winner}`}</div>}
      </div>
    )
  }
}

export default PoleView
