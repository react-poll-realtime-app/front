import React from 'react';
import CreatePollForm from './../../Components/CreatePoll';
import JoinPollForm from './../../Components/JoinPoll';
var api=require('../../helpers/api');
import {enterPoll} from './../../helpers/socket';
//
// import io from 'socket.io-client';
// let socket = io('http://localhost:8080');


const HomeView = (props) => {
  let iName;

  return (
    <div>
      <input ref={r => iName=r}  type="text" placeholder="Nickname" />
      <CreatePollForm onPollCreate = {(desc) => {
        api.createPoll(iName.value, desc)
        .then(a => {
          if (a.data._id){
            props.history.push(`/poll/${a.data._id}`, {myName: iName.value})
          }
        }
      );

      }}/>
      <JoinPollForm onJoinPress={(id) => {
        let myName = iName.value;
        api.joinPoll(id, myName)
        .then(a => {
          if (a.data._id){
            enterPoll(a.data._id, myName);

            props.history.push(`/poll/${a.data._id}`, {myName: myName});
          }
        })
      }} />
    </div>
  )
};

export default HomeView;
